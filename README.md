installing on debian 10:

apt install mc gnupg2 -y
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 049AD65B
echo "deb https://apt.opensips.org buster 3.1-releases" >/etc/apt/sources.list.d/opensips.list
echo "deb https://apt.opensips.org buster cli-releases" >/etc/apt/sources.list.d/opensips-cli.list
apt update
apt upgrade -y
apt install opensips-cli opensips -y

